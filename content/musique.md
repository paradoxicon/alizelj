---
title: "Playlist"
date: 2022-09-21T08:57:12+02:00
description: Pour ambiancer ta lecture !
draft: false
featured_image: 'images/synths.jpg'
omit_header_text: true
type: page
menu: main
---

Intégrer une playlist via Spotify (pas encore trouvé l'astuce pour soundcloud) :

{{< spotify type="album" id="03h9BDKxEwdYRpH5uZ2bS0" width="100%" height="250" >}}
