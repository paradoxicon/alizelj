---
title: "Tux"
date: 2022-09-21T08:07:12+02:00
draft: false
tags:
    - swag
    - pingouins
---
Say hello to Tux!

Tada!

![Tux](/alizelj/images/tux.png "Un pingouin préhistorique de belle facture.")
